﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Assignment1
{
    internal class AddItemCommand : ICommand
    {
        private ObservableCollection<ItemList> lists;

        public AddItemCommand(ObservableCollection<ItemList> lists)
        {
            this.lists = lists;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            lists.Add(new ItemList() {  ItemImage= null, ItemName="Coffee"});
        }
    }
}