﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Assignment1
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<ItemList> Lists { get; set; } = new ObservableCollection<ItemList>();
        public ICommand AddCommand { get; set; }

        public bool IsListFull { get; set; } = true;

        public MainWindowViewModel()
        {
            Lists.Add(new ItemList() { ItemImage = "image/coffee.jpg", ItemName = "Coffee" });
            Lists.Add(new ItemList() { ItemImage = "image/tea.jpg", ItemName = "Tea" });
            Lists.Add(new ItemList() { ItemImage = "image/soda.jpg", ItemName = "Soda" });
            Lists.Add(new ItemList() { ItemImage = "image/beer.jpg", ItemName = "Beer" });
            Lists.Add(new ItemList() { ItemImage = "image/wine.jpg", ItemName = "Wine" });

            Lists.CollectionChanged += Lists_CollectionChanged;

            AddCommand = new AddItemCommand(Lists);

        }

        private void Lists_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Lists.Count >= 5)
            {
                IsListFull = false;
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsListFull)));


        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}